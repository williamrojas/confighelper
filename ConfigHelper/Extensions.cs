﻿

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace BIG.ConfigHelper
{
    public static class Extensions
    {
        /// <summary>
        /// Gets the properties of an object as a url string (separated by '&')
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string GetQueryString(this object obj)
        {
            return string.Join("&", ((IEnumerable<PropertyInfo>)obj.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)).Where<PropertyInfo>((Func<PropertyInfo, bool>)(p => p.GetValue(obj, (object[])null) != null)).Select<PropertyInfo, string>((Func<PropertyInfo, string>)(p => p.Name + "=" + HttpUtility.UrlEncode(p.GetValue(obj, (object[])null).ToString()))).ToArray<string>());
        }

        /// <summary>
        /// Gets the properties of an object as csv
        /// </summary>
        /// <param name="obj">The object</param>
        /// <returns></returns>
        public static string GetProperties(this object obj)
        {
            return string.Join(", ", ((IEnumerable<PropertyInfo>)obj.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)).Where<PropertyInfo>((Func<PropertyInfo, bool>)(p => p.GetValue(obj, (object[])null) != null)).Select<PropertyInfo, string>((Func<PropertyInfo, string>)(p => p.Name + "=" + HttpUtility.UrlEncode(p.GetValue(obj, (object[])null).ToString()))).ToArray<string>());
        }

        public static string GetArgumentValue(string[] args, string key)
        {
            string str = string.Empty;
            IEnumerable<string> source = ((IEnumerable<string>)args).Where<string>((Func<string, bool>)(i => i.Contains(key)));
            if (source.Count<string>() > 0)
                str = source.ElementAt<string>(0).Split('=')[1];
            return str;
        }


        /// <summary>
        /// Parses an exception into a string
        /// </summary>
        /// <param name="ex"></param>
        /// <returns></returns>
        public static string GetExceptionDetailsAsString(this Exception ex)
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.AppendLine("Exception:");
            stringBuilder.AppendLine(" Message:" + ex.Message);
            stringBuilder.AppendLine(" Source:" + ex.Source);
            stringBuilder.AppendLine(" InnerException:" + (object)ex.InnerException);
            stringBuilder.AppendLine(" StackTrace:" + ex.StackTrace);
            stringBuilder.AppendLine("");
            return stringBuilder.ToString();
        }

        /// <summary>
        /// Determines whether a file is locked for write operations
        /// </summary>
        /// <param name="file">The file as FileInfo</param>
        /// <returns></returns>
        public static bool IsFileLocked(FileInfo file)
        {
            FileStream fileStream = (FileStream)null;
            try
            {
                fileStream = file.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
            }
            catch (IOException)
            {
                return true;
            }
            finally
            {
                if (fileStream != null)
                    fileStream.Close();
            }
            return false;
        }

        /// <summary>
        /// Determines whether the specified url contains invlaid characters
        /// </summary>
        /// <param name="WebUrl">The url</param>
        /// <returns></returns>
        public static bool IsValidUrl(string WebUrl)
        {
            return Regex.IsMatch(WebUrl, "(http|https)://([\\w-]+\\.)+[\\w-]+(/[\\w- ./?%&=]*)?");
        }

        /// <summary>
        /// Sets the property of an object
        /// </summary>
        /// <param name="objectToModify">The object to modify</param>
        /// <param name="prop">The property</param>
        /// <param name="stringValue">The new value</param>
        public static void SetPropertyValue(object objectToModify, PropertyInfo prop, string stringValue)
        {
            if (prop.PropertyType.IsEnum)
            {
                object obj = Enum.Parse(prop.PropertyType, stringValue);
                objectToModify.GetType().GetProperty(prop.Name).SetValue(objectToModify, obj, null);
            }
            else if (prop.PropertyType == typeof(int))
            {
                int num = int.Parse(stringValue);
                objectToModify.GetType().GetProperty(prop.Name).SetValue(objectToModify, (object)num, null);
            }
            else if (prop.PropertyType == typeof(double))
            {
                double num = double.Parse(stringValue);
                objectToModify.GetType().GetProperty(prop.Name).SetValue(objectToModify, (object)num, null);
            }
            else if (prop.PropertyType == typeof(string))
                objectToModify.GetType().GetProperty(prop.Name).SetValue(objectToModify, (object)stringValue, null);
            else if (prop.PropertyType == typeof(Uri))
            {
                string uriString = stringValue;
                if (!uriString.EndsWith("/"))
                    uriString += "/";
                Uri uri = new Uri(uriString);
                objectToModify.GetType().GetProperty(prop.Name).SetValue(objectToModify, (object)uri, null);
            }
            else if (prop.PropertyType == typeof(TimeSpan))
            {
                TimeSpan timeSpan = TimeSpan.Parse(stringValue);
                objectToModify.GetType().GetProperty(prop.Name).SetValue(objectToModify, (object)timeSpan, null);
            }
            else
            {
                if (prop.PropertyType != typeof(bool))
                    return;
                string str = stringValue;
                if (str == "1" || str == "0")
                    str = str == "1" ? true.ToString() : false.ToString();
                objectToModify.GetType().GetProperty(prop.Name).SetValue(objectToModify, (object)bool.Parse(str), null);
            }
        }

        /// <summary>
        /// Writes an exception to the windows event log
        /// </summary>
        /// <param name="ex">The exception</param>
        /// <param name="logSource">The log source</param>
        /// <param name="LogName">THe name of the log</param>
        public static void WriteEventLog(Exception ex, string logSource, string LogName)
        {
            if (!EventLog.SourceExists(logSource))
                EventLog.CreateEventSource(logSource, LogName);
            Extensions.WriteEventLog(ex.GetExceptionDetailsAsString(), EventLogEntryType.Error, logSource, LogName);
        }

        /// <summary>
        /// Writes a message to the windows event log
        /// </summary>
        /// <param name="strMessage">THe message</param>
        /// <param name="evntType">Tee type of event</param>
        /// <param name="logSource">The log source</param>
        /// <param name="LogName">THe name of the log</param>
        public static void WriteEventLog(string strMessage, EventLogEntryType evntType, string logSource, string logName)
        {
            if (logName == string.Empty)
                logName = logSource;
            if (!EventLog.SourceExists(logSource))
                EventLog.CreateEventSource(logSource, logName);
            new EventLog(logName)
            {
                Source = logSource
            }.WriteEntry(strMessage, evntType);
        }


    }
}
