﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BIG.ConfigHelper
{
    /// <summary>
    /// Extension of a properties attribute which can be used in conjunction with selfconfigurable class to automatically set the properties values of an object 
    /// </summary>
    public class ProcessAttributes : Attribute
    {
        /// <summary>
        /// Whether the property value is to be set from the config file or not
        /// </summary>
        public bool IsSettingInConfigFile { get; set; }


        /// <summary>
        /// Whether the property is required or not
        /// </summary>
        public bool RequiredInConfigFile { get; set; }


        /// <summary>
        /// Inisitlizes an instance of a process attributes with both IsSettingInConfigFile and RequiredInConfigFile set to true
        /// </summary>
        public ProcessAttributes()
        {
            this.IsSettingInConfigFile = true;
            this.RequiredInConfigFile = true;
        }
    }
}
