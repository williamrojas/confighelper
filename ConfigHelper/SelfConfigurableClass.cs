﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.IO;

namespace BIG.ConfigHelper
{

    /// <summary>
    /// Class helper to automatically set the values of its properties
    /// </summary>
    public abstract class SelfConfigurableClass
    {
        public System.Configuration.Configuration Config;
        public AppSettingsSection AppSettings;

#region Constructors and Destructors
        
        /// <summary>
        /// Class Constructor which automatically sets the config file to [appName].config or [appName].exe.config
        /// </summary>
        public SelfConfigurableClass()
        {
            string configFileName = AppDomain.CurrentDomain.SetupInformation.ConfigurationFile;
            this.SetConfigFile(configFileName);
        }


        /// <summary>
        /// Class Constructor which sets the config file to the specified one
        /// </summary>
        public SelfConfigurableClass(string configFileName)
        {
            this.SetConfigFile(configFileName);
        }

#endregion

        /// <summary>
        /// Sets the config file
        /// </summary>
        /// <param name="configFileName">Full path to the config file</param>
        public void SetConfigFile(string configFileName)
        {
            if (!File.Exists(configFileName))
                throw new Exception("Unable to set the configuration file: " + configFileName);

            this.Config = ConfigurationManager.OpenMappedExeConfiguration(new ExeConfigurationFileMap() { ExeConfigFilename = configFileName }, ConfigurationUserLevel.None);

            this.AppSettings = (AppSettingsSection)this.Config.GetSection("appSettings");

            this.init();
        }


        /// <summary>
        /// Initializes the objects properties
        /// </summary>
        protected void init()
        {
            if (this.AppSettings == null)
                throw new Exception("No configuration file has been set");

            foreach (PropertyInfo prop in ((IEnumerable<PropertyInfo>)this.GetType().GetProperties()).Where<PropertyInfo>((Func<PropertyInfo, bool>)(p => ((IEnumerable<object>)p.GetCustomAttributes(typeof(ProcessAttributes), true)).Where<object>((Func<object, bool>)(ca => ((ProcessAttributes)ca).IsSettingInConfigFile)).Any<object>())))
            {
                if (this.AppSettings.Settings[prop.Name] == null)
                {
                    if (((ProcessAttributes)prop.GetCustomAttributes(typeof(ProcessAttributes), true)[0]).RequiredInConfigFile)
                    {
                        StringBuilder sb = new StringBuilder();
                        sb.AppendLine("Unable to find the following setting: " + prop.Name);
                        sb.AppendLine("Please check the config file and make sure the setting is properly set");
                        throw new Exception(sb.ToString());
                    }
                }
                else
                {
                    try
                    {
                        Extensions.SetPropertyValue((object)this, prop, this.AppSettings.Settings[prop.Name].Value);
                    }
                    catch (Exception ex)
                    {
                        StringBuilder stringBuilder = new StringBuilder();
                        stringBuilder.AppendLine("Error --  unable to parse the following setting: " + prop.Name);
                        stringBuilder.AppendLine("          Please check the config file and make sure the setting is properly set");
                        stringBuilder.AppendLine(ex.Message);
                        stringBuilder.AppendLine(ex.StackTrace);

                        throw new Exception(stringBuilder.ToString());
                    }
                }
            }
        }
    }
}
