﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace BIG.ConfigHelper
{
    /// <summary>
    /// Object to Encrypt/Decrypt bytes using Rijndael encryption algorithm.
    /// </summary>
    public class Encryptor
    {
        System.Text.Encoding _encoding = Encoding.Unicode;

        /// <summary>
        /// Constructor using unicode encoding
        /// </summary>
        public Encryptor(){}

        /// <summary>
        /// Constructor using the specified encoding type
        /// </summary>
        public Encryptor(System.Text.Encoding encoding)
        {
            this._encoding = encoding;
        }
        

        /// <summary>
        /// Encrypts a string using username and password
        /// </summary>
        /// <param name="plainText">The text to encrypt</param>
        /// <param name="password">The password to encrypt</param>
        /// <param name="salt">The salt for the encryption mechanism</param>
        /// <returns>A string of Unicode bytes</returns>
        public  byte[] EncryptStringToBytes(string plainText, string password, string salt)
        {
            Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(password, Encoding.ASCII.GetBytes(salt));
            return EncryptStringToBytes(plainText, pdb.GetBytes(32), pdb.GetBytes(16));
        }


        /// <summary>
        /// Encrypts a string using Secret Key and initialization vector
        /// </summary>
        /// <param name="plainText">The text to encrypt</param>
        /// <param name="Key">Secret key</param>
        /// <param name="IV">Initialization vector</param>
        /// <returns>A string of Unicode bytes</returns>
        public byte[] EncryptStringToBytes(string plainText, byte[] Key, byte[] IV)
        {
            // Check arguments.
            if (plainText == null || plainText.Length <= 0)
                throw new ArgumentNullException("plainText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("IV");
            byte[] encrypted;
            // Create an Rijndael object
            // with the specified key and IV.
            using (Rijndael rijAlg = Rijndael.Create())
            {
                rijAlg.Key = Key;
                rijAlg.IV = IV;

                // Create an encryptor to perform the stream transform.
                ICryptoTransform encryptor = rijAlg.CreateEncryptor(rijAlg.Key, rijAlg.IV);

                // Create the streams used for encryption.
                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt, this._encoding))
                        {

                            //Write all data to the stream.
                            swEncrypt.Write(plainText);
                        }
                        encrypted = msEncrypt.ToArray();
                    }
                }
            }


            // Return the encrypted bytes from the memory stream.
            return encrypted;

        }



        /// <summary>
        /// Decrypts an encrypted text using username and password
        /// </summary>
        /// <param name="plainText"></param>
        /// <param name="Key"><see cref="SymmetricAlgorithm.Key"/></param>
        /// <param name="IV"><see cref="SymmetricAlgorithm.IV"/></param>
        /// <returns></returns>
        public string DecryptStringFromBytes(string encryptedText, string password, string salt)
        {
            Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(password, Encoding.ASCII.GetBytes(salt));
            return DecryptStringFromBytes(this._encoding.GetBytes(encryptedText), pdb.GetBytes(32), pdb.GetBytes(16));
            
        }

        /// <summary>
        /// Decrypts an encrypted array of bytes using username and password
        /// </summary>
        /// <param name="plainText">The encrypted array</param>
        /// <param name="Key"><see cref="SymmetricAlgorithm.Key"/></param>
        /// <param name="IV"><see cref="SymmetricAlgorithm.IV"/></param>
        /// <returns></returns>
        public string DecryptStringFromBytes(byte[] encryptedBytes, string password, string salt)
        {
            Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(password, Encoding.ASCII.GetBytes(salt));
            return DecryptStringFromBytes(encryptedBytes, pdb.GetBytes(32), pdb.GetBytes(16));
        }


        /// <summary>
        /// Decrypts an encrypted array of bytes using Secret Key and initialization vector
        /// </summary>
        /// <param name="cipherText"></param>
        /// <param name="Key"></param>
        /// <param name="IV"></param>
        /// <returns></returns>
        public string DecryptStringFromBytes(byte[] cipherText, byte[] Key, byte[] IV)
        {
            // Check arguments.
            if (cipherText == null || cipherText.Length <= 0)
                throw new ArgumentNullException("cipherText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("IV");

            // Declare the string used to hold
            // the decrypted text.
            string plaintext = null;

            // Create an Rijndael object
            // with the specified key and IV.
            using (Rijndael rijAlg = Rijndael.Create())
            {
                rijAlg.Key = Key;
                rijAlg.IV = IV;

                // Create a decryptor to perform the stream transform.
                ICryptoTransform decryptor = rijAlg.CreateDecryptor(rijAlg.Key, rijAlg.IV);

                // Create the streams used for decryption.
                using (MemoryStream msDecrypt = new MemoryStream(cipherText))
                {
                    using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader srDecrypt = new StreamReader(csDecrypt, this._encoding))
                        {

                            // Read the decrypted bytes from the decrypting stream
                            // and place them in a string.
                            plaintext = srDecrypt.ReadToEnd();
                        }
                    }
                }

            }

            return plaintext;

        }
    
    }
}
