﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BIG.ConfigHelper
{
    /// <summary>
    /// Object that provides a message as a description/type
    /// </summary>
    public class Message
    {
        /// <summary>
        /// Enumeration that describes the type of message
        /// </summary>
        public enum MessageType { INFO, WARNING, ERROR };

        /// <summary>
        /// The message description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// The type of message
        /// </summary>
        public MessageType Type { get; set; }
    }
}
